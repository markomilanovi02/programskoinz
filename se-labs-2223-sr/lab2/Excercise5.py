import json

employees_list = []

with open("/Users/mmilanovic/Documents/proging/se-labs-2223-sr/ex4-employees.json", "r", encoding="utf-8") as f:
  employees = json.load(f)

class Employee:
  def __init__(self, name, title, age, office):
    self.name = name
    self.title = title
    self.age = age
    self.office = office

  def __str__(self):
    return f"{self.name} ({self.age}), {self.title} @ {self.office}"

for e in employees:
  employees_list.append(e)
  print(Employee(e["employee"], e["title"], e["age"], e["office"]))